import axios from 'axios'
import now from 'performance-now';

function dataURItoBlob(dataURI) {
  // convert base64/URLEncoded data component to raw binary data held in a string
  var byteString;
  if (dataURI.split(',')[0].indexOf('base64') >= 0)
      byteString = atob(dataURI.split(',')[1]);
  else
      byteString = unescape(dataURI.split(',')[1]);

  // separate out the mime component
  var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

  // write the bytes of the string to a typed array
  var ia = new Uint8Array(byteString.length);
  for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
  }

  return new Blob([ia], {type:mimeString});
}

export const uploader = async (code) => {
  const blob = await dataURItoBlob(code)
  // const file = new Blob([files[0]], { type: 'image/png' });
  const formData = new FormData();
  formData.append('file', blob, 'jpeg');
  return await axios.post('http://localhost:3000/image', formData)
}

export const searchInSupermarket = async (query) => {
  const { data: { searchWork } } = await axios.get(`http://localhost:3000/translate?text=${query}&target=nl`)
  const { data: { cards } } = await axios.get(`https://www.ah.nl/zoeken/api/products/search?size=36&query=${searchWork}`)
  const { price, images } = cards[0].products[0]
  return {
    price: price.now,
    image: images[0].url
  }
}

  // data:image/jpeg;base64,
  // stream;base64,data:image/png;base64
